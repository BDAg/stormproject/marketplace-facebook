var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var gruposSchema = new Schema
                                (
                                    {
                                        nomeGrupos : { type : String },
                                        linkGrupos : { type : String},
                                        quantidadeOfertas : { type : String},
                                    }
                                );

module.exports = mongoose.model('grupos', gruposSchema, "gruposFacebook");