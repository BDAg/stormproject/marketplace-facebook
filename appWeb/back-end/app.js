const express = require("express")
const app = express();
const bodyParser = require("body-parser");
const morgan = require("morgan");
const mongoose = require("mongoose");

const apiRouter = require("./src/routes/api");
const ofertasFacebook = require("./src/routes/ofertasFacebook");
const gruposFacebook = require('./src/routes/gruposRouter')

mongoose.connect('mongodb+srv://botFacebook:bc123456!@storm-project-eu6jj.mongodb.net/StormProject', { useNewUrlParser: true }); 
mongoose.Promise = global.Promise;

app.use(bodyParser.urlencoded({ extended: true}));
app.use(bodyParser.json());
//app.use(morgan('dev'))

app.use(function (req, res, next) {
    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', '*');
    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    // Pass to next layer of middleware
    next();
});

app.use("/api", apiRouter);

app.use("/ofertasFacebook", ofertasFacebook);

app.use("/gruposFacebook",gruposFacebook)

app.use((req, res, next) => {
    const erro = new Error("Não encontrado");
    erro.status = 404;
    next(erro);
})

app.use((error, req, res, next) => {
    res.status(error.status || 500);
    res.json({
        erro: {
            mensagem: error.message
        }
    })
})

module.exports = app;