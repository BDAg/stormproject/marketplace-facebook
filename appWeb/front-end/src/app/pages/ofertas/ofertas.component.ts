import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-ofertas',
  templateUrl: './ofertas.component.html',
  styleUrls: ['./ofertas.component.css']
})

export class OfertasComponent {

  idOferta: any;
  parametro:any;
  listaItens: any;
  listaPompeia: any;
  listaMarilia: any;
  listaQuintana: any;

  constructor(private http: HttpClient) { }

  listarUsuario() {
    return this.http.get('https://api-storm-project.herokuapp.com/ofertasfacebook/').subscribe((result => {
      this.listaItens = result;
      //console.log(this.listaItens)
    }))
  }

  listarPompeia() {
    return this.http.get('https://api-storm-project.herokuapp.com/ofertasfacebook/grupo/5bd0af086db3c71378ceaebe').subscribe((result => {
      this.listaPompeia = result;
      //console.log(this.listaPompeia)
    }))
  }

  listarMarilia() {
    return this.http.get('https://api-storm-project.herokuapp.com/ofertasfacebook/grupo/5bd0af086db3c71378ceaebf').subscribe((result => {
      this.listaMarilia = result;
      //console.log(this.listaMarilia)
    }))
  }

  listarQuintana() {
    return this.http.get('https://api-storm-project.herokuapp.com/ofertasfacebook/grupo/5bd0af296db3c71378ceaec4').subscribe((result => {
      this.listaQuintana = result;
      //console.log(this.listaQuintana)
    }))
  }
  
  ngOnInit() {
    this.listarUsuario();
    this.listarPompeia();
    this.listarMarilia();
    this.listarQuintana();
    this.exibirMensagem('todasOfertas')
  }

  exibirMensagem(idTag) {
    const mensagem = document.getElementById(idTag);
    mensagem.hidden = false;
  }

  ocultarMensagem(idTag) {
    const mensagem = document.getElementById(idTag);
    mensagem.hidden = true;
  }

  switchHtml(ids) {
    const oldDiv = document.getElementsByClassName('opc');
    for (var i = 0; i < oldDiv.length; i++) {
      oldDiv[i].className = 'btn btn-secondary';
    }

    const div = document.getElementById(ids);
    div.className = 'btn btn-secondary active opc';
    if (ids == 'option1') {
      this.ocultarMensagem('grupo1');
      this.ocultarMensagem('grupo2');
      this.ocultarMensagem('grupo3');
      this.exibirMensagem('todasOfertas');
    } else if (ids == 'option2') {
      this.ocultarMensagem('grupo2');
      this.ocultarMensagem('grupo3');
      this.ocultarMensagem('todasOfertas');
      this.exibirMensagem('grupo1');
    } else if (ids == 'option3') {
      this.ocultarMensagem('grupo1');
      this.ocultarMensagem('grupo3');
      this.ocultarMensagem('todasOfertas');
      this.exibirMensagem('grupo2');
    } else if (ids == 'option4') {
      this.ocultarMensagem('grupo1');
      this.ocultarMensagem('grupo2');
      this.ocultarMensagem('todasOfertas');
      this.exibirMensagem('grupo3');
    }
  }

}
