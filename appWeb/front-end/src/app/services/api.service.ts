import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http: HttpClient) { }

  logar(pessoa) {
    return this.http.post('https://api-storm-project.herokuapp.com/api/login', pessoa);
  }

  cadastrar(pessoa) {
    return this.http.post('https://api-storm-project.herokuapp.com/api/cadastrar', pessoa);
  }

  recuperarSenha(token, novaSenha) {
    return this.http.post('https://api-storm-project.herokuapp.com/api/recuperar_senha', { 'token': token, 'novaSenha': novaSenha });
  }

  esqueceuSenha(email) {
    return this.http.post('http://api-storm-project.herokuapp.com/api/esqueceusenha', { 'email': email });
  }

  redefinirSenha(dados) {
    return this.http.post('https://api-storm-project.herokuapp.com/api/redefinirsenha', dados);
  }

  editarRegistro(dados) {
    return this.http.put('https://api-storm-project.herokuapp.com/crudusuario/', dados);
  }

  deletarUsuario(email, senha) {
    return this.http.post('https://api-storm-project.herokuapp.com/api/usuarios',{ 'email': email, 'senha' : senha });
  }
}